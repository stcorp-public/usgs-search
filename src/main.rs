use std::fs::File;
use std::io::Write;
use std::path::PathBuf;
use std::string::ToString;

use anyhow::{anyhow, Context, Result};
use chrono::NaiveDate;
use serde_json::to_string_pretty;
use structopt::StructOpt;

use usgs_eros_client::endpoints::{DatasetSearchBuilder, SceneSearchBuilder};
use usgs_eros_client::types::{
    ApiData, CloudCoverFilter, Credentials, DateLike, DateRange, GeoJson, SpatialFilter,
    TemporalFilter,
};
use usgs_eros_client::Client;

#[tokio::main]
async fn main() -> Result<()> {
    let config = Config::from_args();

    match config {
        Config::Scene(scene_config) => handle_scene_subcommand(scene_config).await?,
        Config::Dataset(ds_config) => handle_dataset_subcommand(ds_config).await?,
    };
    Ok(())
}

async fn handle_scene_subcommand(config: SceneConfig) -> Result<()> {
    if config.datasets.len() == 0 {
        return Err(anyhow!(
            "At least one dataset name expected! See ./usgs-search scene --help"
        ));
    }

    let temporal_filter = TemporalFilter::new(
        &format!("{}", config.start_date.format("%Y-%m-%d")),
        &format!("{}", config.end_date.format("%Y-%m-%d")),
    )?;
    let geojson = GeoJson::from_file(config.region_file.as_path())
        .context("Couldn't deserialize the geojson file.")?;
    let spatial_filter = SpatialFilter::geojson(geojson);
    let cloud_filter = CloudCoverFilter::new(0, config.max_cloud, true);

    let credentials = Credentials::from_env()?;
    let client = Client::new(&credentials).await?;

    let mut output_scenes = Vec::<String>::new();

    for dataset in config.datasets {
        let res = client
            .scene_search(&dataset)
            .acquisition_filter(temporal_filter.clone())
            .spatial_filter(spatial_filter.clone())
            .cloud_cover_filter(cloud_filter.clone())
            .max_results(config.max_results)
            .call()
            .await?;

        match res.error_code {
            Some(_) => return Err(anyhow!("API returned an error: {:?}", res)),
            None => (),
        }

        let search_result = match res.data {
            Some(ApiData::SceneSearch(val)) => val,
            _ => return Err(anyhow!("Unexpected API response data {:?}", res.data)),
        };

        if search_result.records_returned == config.max_results {
            println!(
                "WARNING! The amount of scenes returned is equal to `max-results`, try increasing
 `max-result` to see if there are more scenes and make this warning go away."
            );
        }

        let mut scene_ids: Vec<String> = search_result
            .results
            .into_iter()
            .map(|scene| scene.display_id)
            .collect();

        output_scenes.append(&mut scene_ids);
    }

    let mut buffer = [0; 2];
    let output = output_scenes.join(config.separator.encode_utf8(&mut buffer));

    match config.output_file {
        None => println!("{}", output),
        Some(filepath) => {
            let mut file = File::create(filepath)?;
            file.write_all(output.as_bytes())?;
        }
    }
    Ok(())
}

async fn handle_dataset_subcommand(config: DatasetConfig) -> Result<()> {
    let credentials = Credentials::from_env()?;
    let client = Client::new(&credentials).await?;

    let mut request_builder = client.dataset_search();
    request_builder.dataset_name(&config.name);

    match (config.start_date, config.end_date) {
        (Some(start_date), Some(end_date)) => {
            let date_range = DateRange {
                start_date: DateLike::Date(start_date),
                end_date: DateLike::Date(end_date),
            };
            request_builder.temporal_filter(date_range);
        }
        _ => (),
    }

    match config.region_file {
        Some(region_file) => {
            let geojson = GeoJson::from_file(region_file.as_path())
                .context("Couldn't deserialize the geojson file.")?;
            let spatial_filter = SpatialFilter::geojson(geojson);
            request_builder.spatial_filter(spatial_filter);
        }
        None => (),
    }

    let res = request_builder.call().await?;

    match res.error_code {
        Some(_) => return Err(anyhow!("API returned an error: {:?}", res)),
        None => (),
    };

    let datasets = match res.data {
        Some(ApiData::Datasets(val)) => val,
        _ => return Err(anyhow!("Unexpected API response data {:?}", res.data)),
    };

    let output = match config.full_output {
        true => to_string_pretty(&datasets)?,
        false => {
            let dataset_aliases: Vec<String> = datasets
                .into_iter()
                .map(|dataset| dataset.dataset_alias)
                .collect();

            let mut buffer = [0; 2];
            dataset_aliases.join(config.separator.encode_utf8(&mut buffer))
        }
    };

    match config.output_file {
        None => println!("{}", output),
        Some(filepath) => {
            let mut file = File::create(filepath)?;
            file.write_all(output.as_bytes())?;
        }
    }
    Ok(())
}

/// A tool for searching USGS satellite data repositories
#[derive(StructOpt, Debug)]
#[structopt(name = "usgs-search")]
enum Config {
    Scene(SceneConfig),
    Dataset(DatasetConfig),
}

/// Search for scenes
#[derive(StructOpt, Debug)]
struct SceneConfig {
    /// Start date
    #[structopt(short, long)]
    start_date: NaiveDate,
    /// End date
    #[structopt(short, long)]
    end_date: NaiveDate,
    /// Region of interest geojson file
    #[structopt(short, long, parse(from_os_str))]
    region_file: PathBuf,
    /// Dataset names to search for scenes
    datasets: Vec<String>,
    /// Output scene ID separator
    #[structopt(long, default_value = "\n")]
    separator: char,
    /// Output filename to write, if not given, the output is written to stdout.
    #[structopt(short, long, parse(from_os_str))]
    output_file: Option<PathBuf>,
    /// Maximum cloud cover
    #[structopt(long, default_value = "25")]
    max_cloud: u8,
    /// Maximum amount of scenes to return
    #[structopt(long, default_value = "100")]
    max_results: u16,
}

/// Search for datasets
#[derive(StructOpt, Debug)]
struct DatasetConfig {
    /// Dataset search start date
    #[structopt(short, long, requires("end_date"))]
    start_date: Option<NaiveDate>,
    /// Dataset search end date
    #[structopt(short, long, requires("start_date"))]
    end_date: Option<NaiveDate>,
    /// Region of interest geojson file
    #[structopt(short, long, parse(from_os_str))]
    region_file: Option<PathBuf>,
    /// Dataset name to search for. Can be partial.
    name: String,
    /// Output dataset alias separator
    #[structopt(long, default_value = "\n")]
    separator: char,
    /// Output filename to write, if not given, the output is written to stdout.
    #[structopt(short, long, parse(from_os_str))]
    output_file: Option<PathBuf>,
    /// Print the full dataset information as returned by the API, not just dataset aliases.
    #[structopt(short, long)]
    full_output: bool,
}
