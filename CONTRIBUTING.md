# Contributing

1. Get [rust](https://www.rust-lang.org/tools/install)
2. Get [pre-commit](https://pre-commit.com/#install)

Happy hacking!
