# USGS search tool

A tool for searching the USGS satellite data repositories.

Search for datasets:
```
./usgs-search dataset lsr_landsat
```

Yields:
```
lsr_landsat_8_c1
lsr_landsat_etm_c1
lsr_landsat_tm_c1
```

Pass the `--full-output` flag to get the full dataset information.

Search for scenes:
```
./usgs-search scene -s 2018-10-01 -e 2018-10-30 -r ~/vestfold.geojson --max-cloud 10 lsr_landsat_8_c1 lsr_landsat_etm_c1 sentinel_2a
```

Yields:
```
LC08_L1TP_197018_20181023_20181031_01_T1
LC08_L1TP_197019_20181023_20181031_01_T1
LC08_L1TP_197018_20181007_20181029_01_T1
LE07_L1TP_196019_20181024_20181119_01_T1
L1C_T32VNL_A017494_20181028T105138
L1C_T32VNL_A008571_20181027T103123
L1C_T32VNM_A008514_20181023T105107
L1C_T32VNL_A017408_20181022T103053
L1C_T32VNM_A017351_20181018T105349
L1C_T32VNL_A017265_20181012T103019
L1C_T32VNL_A008328_20181010T104018
L1C_T32VNM_A008328_20181010T104018
L1C_T32VNL_A008228_20181003T105022
L1C_T32VNM_A008228_20181003T105022
```

For more see:
```
$ ./usgs-search --help
```

![](st_logo.svg)

Made by: [S&T Norway](https://www.stcorp.no)
